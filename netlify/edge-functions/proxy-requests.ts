import type { Context, Config } from "@netlify/edge-functions";

export default async (request: Request, context: Context) => {

  const ip = request.headers.get('cf-connecting-ip') || request.headers.get('x-forwarded-for') || (request.socket && request.socket.remoteAddress);
  let url = new URL(request.url);
  const worker_domain=url.hostname;
  url.hostname = "wpm-worker.mohsenyz.ir";     
  url.protocol = request.headers.get('x-forwarded-proto') || "https";
  let req = new Request(url, request);
  if (ip)
    request.headers.set('cf-connecting-ip', ip);
    request.headers.set('Host', worker_domain);
  
  return await fetch(req)
};

export const config: Config = {
  path: "/*",
};